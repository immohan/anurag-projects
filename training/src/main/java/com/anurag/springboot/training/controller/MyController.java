package com.anurag.springboot.training.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.anurag.springboot.training.BusinessService;
import com.anurag.springboot.training.model.Greeting;
import com.anurag.springboot.training.model.User;
import com.anurag.springboot.training.model.UserRepository;

@RestController


public class MyController {
	
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    
	@Autowired
	//dependency injection
	BusinessService businessService;
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping(path="/add") 
	public @ResponseBody String  addNewUser(@RequestParam String name
			, @RequestParam String email) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		
		User user = new User();
		user.setName(name);
		user.setEmail(email);
		userRepository.save(user);
		return "SAVED";
	}

	@GetMapping(path="/all")
	public @ResponseBody Iterable<User> getAllUsers() {
		// This returns a JSON or XML with the users
		return userRepository.findAll();
	}
    
    @RequestMapping("/greetings")
	public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
	        return new Greeting(counter.incrementAndGet(),
	                            String.format(template, name));
	 }
    
    @RequestMapping("/goodmorning")
    public String getString() {
        return businessService.getString();
    }
}
